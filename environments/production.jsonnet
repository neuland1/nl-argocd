local template = import '../templates/application.jsonnet';
template.generate(
    hostname='neuland',
    name='production',
    revision='v0.5.0',
    imageTag='v0.5.0'
)
