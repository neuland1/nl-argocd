{
    generate(hostname, name, revision, imageTag): {
        apiVersion: "argoproj.io/v1alpha1",
        kind: "Application",
        metadata: {
            name: name,
            namespace: "argocd",
            finalizers: ["resources-finalizer.argocd.argoproj.io"]
        },
        spec: {
            project: "neuland",
            source: {
                repoURL: "https://gitlab.com/neuland1/nl-app.git",
                targetRevision: revision,
                path: "deployment",
                helm: {
                    parameters: [
                        { name: "hostname", value: hostname },
                        { name: "imageTag", value: imageTag },
                        { name: "name", value: name }
                    ]
                }
            },
            destination: {
                server: "https://kubernetes.default.svc",
                namespace: name
            },
            syncPolicy: {
                automated: {
                    prune: true,
                    selfHeal: true,
                    allowEmpty: true
                },
                syncOptions: [
                    "Validate=false",
                    "CreateNamespace=true",
                    "PrunePropagationPolicy=foreground",
                    "PruneLast=true"
                ],
                retry: {
                    limit: 5,
                    backoff: {
                        duration :"5s",
                        factor: 2,
                        maxDuration: "3m"
                    }
                }
            }
        }
    }
}
